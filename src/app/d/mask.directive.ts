import { Directive, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appMask]'
})
export class MaskDirective implements OnInit, OnDestroy {
  private _preValue: string;
  private _sub: Subscription;

  @Input() set preValue(value: string) {
    this._preValue = value || '00:00';
  }

  constructor(private _timeControl: NgControl) { }

  ngOnInit() {
     this.timeValidate();
  }

  ngOnDestroy() {
    this._sub.unsubscribe();
  }

  timeValidate() {
    this._sub = this._timeControl.control.valueChanges.subscribe(data => {
      let preInputValue: string = this._preValue;
      if (!preInputValue) {
        return;
      }

      let newVal = data.toString().replace(/\D/g, '');

      if (preInputValue && data.length < preInputValue.length) {
        if (newVal.length === 0) {
          newVal = '';
        } else if (newVal.length <= 2) {
          newVal = newVal.replace(/^(\d{0,2})/, '$1');
        } else if (newVal.length <= 4) {
          newVal = newVal.replace(/^(\d{0,2})(\d{0,2})/, '$1:$2');
        }

        this._timeControl.control.setValue(newVal, {emitEvent: false});
      } else {
      if (newVal.length === 0) {
        newVal = '';
      } else if (newVal.length <= 2) {
        newVal = newVal.replace(/^(\d{0,2})/, '$1');
      } else if (newVal.length <= 4) {
        newVal = newVal.replace(/^(\d{0,2})(\d{0,2})/, '$1:$2');
      }
      this._timeControl.control.setValue(newVal, {emitEvent: false});
    }
  });
 }
}
