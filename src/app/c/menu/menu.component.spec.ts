import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuComponent } from './menu.component';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on click event', () => {
    let open = false;
    beforeEach(() => {
      open = false;
      component.open = open;
    });

    describe('on button click', () => {
      it('should toggle menu', () => {
        component.toggleMenu();

        expect(component.open).toBeTruthy();
      });
    });

    describe('on outside click', () => {
      it('should close menu', () => {
        component.clickOut(new Event('click'));

        expect(component.open).toBeFalsy();
      });
    });
  });
});
