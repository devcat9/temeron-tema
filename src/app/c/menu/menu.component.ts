import { Component, Input, HostListener, ElementRef } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  @Input() menuItems: Array<any>;
  @Input() menuClass: string;
  open = false;

  constructor(private elRef: ElementRef) { }

  @HostListener('document:click', ['$event']) clickOut(event) {
    if (!this.elRef.nativeElement.contains(event.target)) {
      this.open = false;
    }
  }

  toggleMenu() {
    this.open = !this.open;
  }

  executeFunc(funcStr: string) {
    try {
      new Function(funcStr)();
    } catch (error) {
      console.log(error);
    }
  }

}
