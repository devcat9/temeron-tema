import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import * as moment from 'moment';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})
export class ProgressComponent implements OnInit {
  dashoffset: number;
  value: number;
  radius = 54;
  circumference = 2 * Math.PI * this.radius;
  exeeds = false;

  pieHovered = false;
  inputFocused = false;

  estimatedFControl: FormControl = new FormControl('00:00');
  completedFControl: FormControl = new FormControl('00:00');
  remainingFControl: FormControl = new FormControl('00:00');
  exceededFControl: FormControl = new FormControl('00:00');

  menuItems = [
    { title: 'Adjust estimate', func: 'alert("Adjust estimate")' }
  ];

  constructor() { }

  ngOnInit() {
    combineLatest(this.estimatedFControl.valueChanges, this.completedFControl.valueChanges).subscribe(d => {
      this.calculateDiff(this.estimatedFControl.value, this.completedFControl.value);
    });
  }

  progress(value: number) {
    const progress = value / 100;
    this.dashoffset = this.circumference * (1 - progress);
  }

  calculateDiff(est, comp) {
    est = (est !== '00:00' ? est : '24:00');
    comp = (comp !== '00:00' ? comp : '24:00');
    this.remainingFControl.setValue(moment.utc(moment(est, 'HH:mm').diff(moment(comp, 'HH:mm'))).format('HH:mm'));
    if ((this.totalSeconds(comp) / this.totalSeconds(est) * 100) > 100) {
      this.exceededFControl.setValue(moment.utc(moment(comp, 'HH:mm').diff(moment(est, 'HH:mm'))).format('HH:mm'));
      this.exeeds = true;
    } else {
      this.exeeds = false;
    }

    this.progress(this.totalSeconds(comp) / this.totalSeconds(est) * 100);
  }

  totalSeconds(time) {
    let parts = time.split(':');
    return parts[0] * 3600 + parts[1] * 60;
  }
}
