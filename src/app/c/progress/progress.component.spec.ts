import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressComponent } from './progress.component';
import { MenuComponent } from '../menu/menu.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaskDirective } from 'src/app/d/mask.directive';

describe('ProgressComponent', () => {
  let component: ProgressComponent;
  let fixture: ComponentFixture<ProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        ProgressComponent,
        MenuComponent,
        MaskDirective
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when time difference is -1 hour', () => {
    it('should calculate difference between estimated and completed values', () => {
      const oneHourDashoffset = '26';

      component.calculateDiff('13:00', '12:00');

      expect(component.dashoffset.toFixed(0)).toBe(oneHourDashoffset);
    });
  });

  describe('when time difference is +1 hour', () => {
    it('should calculate difference between estimated and completed values', () => {
      const oneHourDashoffset = '-26';

      component.calculateDiff('13:00', '14:00');
      expect(component.dashoffset.toFixed(0)).toBe(oneHourDashoffset);
    });
  });
});
